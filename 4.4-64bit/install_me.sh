#!/bin/bash 

# the output of dpkg --get-selections  should be piped into this script:

# i.e. the file selections.txt in this folder.  Install the ubuntu ARM compilers by
# using the ubuntu-arm-gcc.txt file
# example:
# sudo ./install_server_packages.sh < selections.txt


rm -f tmp.txt tmp2.txt

touch tmp
touch tmp2

while read line
do
  echo "$line"
  echo "$line" | grep -v deinstall | grep -v "^$" | grep -v "^#" | awk '{print $1}' >> tmp.txt
  echo "$line" | grep deinstall    | grep -v "^$" | grep -v "^#" | awk '{print $1}' >> tmp2.txt
done < "${1:-/dev/stdin}"


AllPackages=""

while read line ; do
    echo "Adding package $line"
    AllPackages=${AllPackages}" "$line
done < tmp.txt

echo "Installing packages......................."

# check if the list is empty or only contains blanks

Stripped="${AllPackages// }"

if [ -z ${Stripped}  ]   ; then
    echo "No packages to install !! "
else
    apt-get install -y $AllPackages
fi


# clear up any dependency issues
apt-get -f install

AllPackages=""

while read line ; do
    AllPackages=${AllPackages}" "$line
done < tmp2.txt

echo "Removing packages......................."

Stripped="${AllPackages// }"

if [ -z ${Stripped}  ]   ; then
    echo "No packages to install !! "
else
    apt-get purge -y $AllPackages
fi



echo "Auto remove............................"

apt-get autoremove -y

echo "Apt clean ............................."
apt-get clean


