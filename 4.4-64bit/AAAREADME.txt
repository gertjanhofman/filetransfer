64-bit Cross Compilers
======================

These are the same as in the 4.4 folder except for running on a 64-bit host.

We can't use Ubuntu cross compiler because it links against glibc 2.17 
which wont be on the wheezy based EDAQ ARM platform. Try to install Squeeze cross toolchain (recommend in Debian web page)

temp move over sources.list and add only:

deb http://www.emdebian.org/debian squeeze main

Then,

Had to install this deb libgmp3c2_4.3.2+dfsg-2ubuntu1_i386.deb  which is no longer in raring (was even in Quantal). 
Got it from launchpad: https://launchpad.net/ubuntu/raring/+package/libgmp3c2 . Now in SVN under server. Need the equivalent package for 64 bit


apt-get install emdebian-archive-keyring
apt-get update
apt-get install  gcc-4.4-arm-linux-gnueabi  g++-4.4-arm-linux-gnueabi 

This pulls in the following:

binutils-arm-linux-gnueabi
cpp-4.4-arm-linux-gnueabi 
gcc-4.4-base-armel-cross 
gcc-4.4-arm-linux-gnueabi-base
gcc-4.4-arm-linux-gnueabi 
g++-4.4-arm-linux-gnueabi 
libc6-armel-cross
libc-dev-bin-armel-cross
libc-bin-armel-cross 
libgcc1-armel-cross
libgomp1-armel-cross
libc-dev-bin-armel-cross
libstdc++6-armel-cross
libstdc++6-4.4-dev-armel-cross
linux-libc-dev-armel-cross
libc6-dev-armel-cross

Now you want to make sure these packages are pinned. See the hold_packages.sh script:
  apt-mark hold <package_name>



To remove:
sudo remove_me.sh < list_of_files.txt

To install:
sudo dpkg -i *.deb

