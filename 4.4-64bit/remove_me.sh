#!/bin/bash 

# the output of dpkg --get-selections  should be piped into this script:

# i.e. the file selections.txt in this folder.  Install the ubuntu ARM compilers by
# using the ubuntu-arm-gcc.txt file
# example:
# sudo ./install_server_packages.sh < selections.txt


AllPackages=""

while read line
do
   
    Package=$(echo $line | awk '{print $1}')
    if dpkg --list | grep $Package; then
        echo "Adding package $line"
	AllPackages=${AllPackages}" "$(echo $line | awk '{print $1}')
    fi	
done < "${1:-/dev/stdin}"

echo "Removing packages......................."


Stripped="${AllPackages// }"

echo "they are: "
echo $AllPackages
echo "---"

if [ -z ${Stripped}  ]   ; then
    echo "No packages to remove !! "
else
    apt-get purge -m -y $AllPackages
fi

echo "Auto remove............................"

apt-get autoremove -y

echo "Apt clean ............................."
apt-get clean


