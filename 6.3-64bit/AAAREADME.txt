For Ubuntu 16.04:
=====================================================================
Make sure the intel gcc and libmpc3 are installed
sudo apt-get install build-essential
sudo apt-get install libmpc3

Then install all the files in this folder with
sudo dpkg -i --force-all *.deb

Then add the header files and libraries required for the USB devices:

copy the two libs  to /usr/arm-linux-gnueabi/lib/  and create the appropriate sym links to  libudev.so  etc.

copy the two header files to /usr/arm-linux-gnueabi/include/





64-bit Cross Compilers
=====================================================================

These are from the stretch release of debian, downloaded from debian

__THIS__ is what we are using for the R300 release on  64-bit 18.04

We can't use Ubuntu cross compiler because it links against glibc 2.17 

To remove:

sudo ./remove_me.sh < list_of_files.txt

To install (old, see below)
---------------------------
sudo dpkg --force-depends-version  -i  *.deb
sudo apt-get -f install
(note the above gives a problem, pulling in a 2.27 version of a Ubuntu cross
file that gives executables 2.27 glibc dependencies as see with e.g.:
/usr/bin/arm-linux-gnueabi-readelf -a  ptpd2.arm   | grep 2.27


Pin the packages:

apt-mark hold binutils-arm-linux-gnueabi           
apt-mark hold cpp-6-arm-linux-gnueabi              
apt-mark hold g++-6-arm-linux-gnueabi              
apt-mark hold gcc-6-arm-linux-gnueabi-base         
apt-mark hold gcc-6-arm-linux-gnueabi              
apt-mark hold gcc-6-cross-base                     
apt-mark hold libasan3-armel-cross                 
apt-mark hold libatomic1-armel-cross              
apt-mark hold libc6-armel-cross                   
apt-mark hold libc6-dev-armel-cross               
apt-mark hold libgcc-6-dev-armel-cross            
apt-mark hold libgcc1-armel-cross                
apt-mark hold libgmp3c2                          
apt-mark hold libgomp1-armel-cross              
apt-mark hold libisl15                          
apt-mark hold libmpfr4                          
apt-mark hold libstdc++-6-dev-armel-cross       
apt-mark hold libstdc++6-armel-cross            
apt-mark hold libubsan0-armel-cross             
apt-mark hold linux-libc-dev-armel-cross



Update:
----------------------------
Have reconfigured the binutils-arm-linux-gnueabi package to not require exact
version of binutils but anything over >2.28. This was done using:

ar x binutils-arm-linux-gnueabi_2.28-5_amd64.deb

drwxr-xr-x 2 ghofman ghofman    4096 Jun 15 12:56 ./
drwxr-xr-x 3 ghofman ghofman    4096 Jun 15 12:48 ../
-rw-r--r-- 1 ghofman ghofman 2754380 Jun 15 12:55 binutils-arm-linux-gnueabi_2.$
-rw-r--r-- 1 ghofman ghofman     627 Jun 15 12:56 control.tar.gz
-rw-r--r-- 1 ghofman ghofman 2753560 Jun 15 12:56 data.tar.xz
-rw-r--r-- 1 ghofman ghofman       4 Jun 15 12:56 debian-binary


gunzip control.tar.gz ; tar -tf control.tar

./
./control
./shlibs
./triggers

tar -xf control.tar
emacs control

# loging in as root here

chown root:root *

# recreate the control file
tar c ./control ./shlibs ./triggers | gzip -c > control.tar.gz

ar rcs newpackage.deb debian-binary control.tar.gz data.tar.xz

Note the order of the ar command is important. See also:

https://coderwall.com/p/hes3ha/change-the-dependencies-of-a-deb-package

======================================================


From the server install script:

dpkg -i 6.3-64bit/*.deb
cat list_of_files.txt | awk '{print $1}' | xargs apt-mark hold 
sudo apt-get -f install





